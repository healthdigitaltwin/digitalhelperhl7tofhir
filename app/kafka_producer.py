# ./app/kafka_producer.py

from aiokafka import AIOKafkaProducer
from app.logger import get_logger
from aiokafka.errors import KafkaError, KafkaConnectionError
from app.ssl_context import create_ssl_context

async def create_producer(config):
    logger = get_logger(__name__, config)
    ssl_context = create_ssl_context(config, logger)
    try:
        producer = AIOKafkaProducer(
            bootstrap_servers=config["Kafka"]["Brokers"],
            security_protocol="SSL" if ssl_context else "PLAINTEXT",
            ssl_context=ssl_context
        )

        await producer.start()
        return producer
    except Exception as e:
        logger.error(f"An error occurred when creating the Kafka producer: {str(e)}")
        raise

async def send_to_kafka(message, topic, producer, config):
    logger = get_logger(__name__, config)
    try:
        if logger.isEnabledFor(logger.DEBUG):
            logger.debug(f"Sending FHIR message to Kafka topic {topic}: {message}")
        await producer.send_and_wait(topic, message)
        if logger.isEnabledFor(logger.DEBUG):
            logger.debug(f"FHIR message successfully written to Kafka topic {topic}")

    except Exception as e:
        logger.error(f"Failed to send message to Kafka: {e}")
        raise e

