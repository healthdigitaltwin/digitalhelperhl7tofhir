#./app/kafka_consumer.py

import os
from aiokafka import AIOKafkaConsumer, TopicPartition, OffsetAndMetadata
from app.logger import get_logger
from hl7apy.parser import parse_message
from app.rest_client import convert_hl7_to_fhir
from config.config import get_config

config = get_config()

logger = get_logger(__name__, config)


async def start_kafka_consumer(config, producer):
    consumer = await create_kafka_consumer(config)
    checkpoint_file = "checkpoint.txt"

    try:
        logger.info("Starting Kafka consumer...")

        if os.path.exists(checkpoint_file):
            with open(checkpoint_file, "r") as file:
                offset = int(file.read().strip())
            tp = TopicPartition(config["Kafka"]["TopicInboundHL7"], 0)
            consumer.seek(tp, offset)
        
        logger.info("With the completion of the application's startup, a symphony of possibilities begins to unfold. Like a well-tuned instrument, you stand at the precipice of creation, ready to compose a masterpiece of innovation and impact. May your journey be harmonious, your vision be clear, and your determination be unwavering as you rock and roll towards a future filled with boundless achievements.")
        async for message in consumer:
            
            hl7_message = message.value
            
            parsed_message = parse_message(hl7_message)

            tenant = parsed_message.MSH.sending_facility.value
            message_id = parsed_message.MSH.message_control_id.value
            message_type = parsed_message.MSH.message_type.value

            logger.info(f"Received message from Tenant: {tenant} Message Type: {message_type} Message Id: {message_id}| Topic: {message.topic} | Partition: {message.partition} | Offset: {message.offset}")

            logger.debug(f"Received HL7 message: {hl7_message}")

            logger.info(f"Converting message: {tenant}-{message_id}")

            fhir_message = await convert_hl7_to_fhir(hl7_message)

            logger.debug(f"Converted FHIR message: {fhir_message}")
            logger.info(f"Message converted: {tenant}-{message_id}")

            await send_to_kafka(fhir_message, config["Kafka"]["TopicInboundFHIR"], producer, config)

            # Store the last committed offset
            with open(checkpoint_file, "w") as file:
                file.write(str(message.offset + 1))

    except Exception as e:
        logger.error(f"Error occurred while consuming HL7 messages: {str(e)}")
        raise e
    finally:
        await consumer.stop()
        logger.info("Kafka consumer stopped")


async def create_kafka_consumer(config):
    consumer_group = config["Kafka"]["ConsumerGroup"]

    consumer = AIOKafkaConsumer(
        config["Kafka"]["TopicInboundHL7"],
        bootstrap_servers=config["Kafka"]["Brokers"],
        group_id=consumer_group,
        auto_offset_reset="earliest",
        enable_auto_commit=False,
        value_deserializer=lambda v: v.decode("utf-8"),
    )

    await consumer.start()
    return consumer


async def send_to_kafka(message, topic, producer, config):
    try:
        logger.debug(f"Sending FHIR message to Kafka topic {topic}: {message}")
        record_metadata = await producer.send_and_wait(topic, message.encode("utf-8"))
        logger.debug(f"FHIR message successfully written to Kafka topic {topic}")

        # Access the record metadata
        topic = record_metadata.topic
        partition = record_metadata.partition
        offset = record_metadata.offset
        timestamp = record_metadata.timestamp

        logger.info(f"Message sent: Topic={topic}, Partition={partition}, Offset={offset}, Timestamp={timestamp}")
    except Exception as e:
        logger.error(f"Failed to send message to Kafka: {e}")
        raise e
