#./app/ssl_context.py

import ssl
from app.logger import get_logger

def create_ssl_context(config, logger):
    ssl_context = None
    if config["Kafka"]["EnableSSL"] == "True":
        ssl_context = ssl.create_default_context(
            purpose=ssl.Purpose.CLIENT_AUTH,
            cafile=config["Kafka"]["CACert"]
        )
        ssl_context.load_cert_chain(
            certfile=config["Kafka"]["ClientCert"],
            keyfile=config["Kafka"]["ClientKey"]
        )
        logger.info("Initialized SSL context for Kafka connection")
    return ssl_context
