#./app/logger.py

import logging
from config.config import get_config

def get_logger(name: str, config):
    logger = logging.getLogger(name)
    level = config["Logging"]["level"]

    if (logger.hasHandlers()):
        logger.handlers.clear()

    try:
        logger.setLevel(level.upper())
    except ValueError:
        raise ValueError(f"Invalid log level: {level}. Please set to one of: DEBUG, INFO, WARNING, ERROR, CRITICAL")

    formatter = logging.Formatter("%(levelname)s:     %(name)s:  %(message)s")

    c_handler = logging.StreamHandler()
    c_handler.setLevel(level.upper())
    c_handler.setFormatter(formatter)

    logger.addHandler(c_handler)

    return logger
