# ./app/rest_client.py
import aiohttp

from config.config import get_config
from app.logger import get_logger

config = get_config()
logger = get_logger(__name__, config=config)

async def convert_hl7_to_fhir(hl7_message):
    logger.debug(f"Sending HL7 message to API: {hl7_message}")
    headers = {'Content-Type': 'text/plain'}
    async with aiohttp.ClientSession() as session:
        try:
            async with session.post(config["RestAPI"]["Url"], data=hl7_message, headers=headers) as resp:
                fhir_message = await resp.text()
                logger.debug(f"Received FHIR message from API: {fhir_message}")
                return fhir_message
        except Exception as e:
            logger.error(f"Error occurred while sending HL7 message to API: {str(e)}")
            raise e
