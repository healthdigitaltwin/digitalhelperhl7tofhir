# main.py

import asyncio
import signal
from app.kafka_producer import create_producer
from app.kafka_consumer import start_kafka_consumer
from app.logger import get_logger
from config.config import get_config

config = get_config()
logger = get_logger(__name__, config)

is_running = True
consumer = None

async def main():
    global is_running, consumer
    logger.info("Starting the application...")
    try:
        producer = await create_producer(config)
        logger.info("Kafka producer created")
        consumer = await start_kafka_consumer(config, producer)
        while is_running:
            await asyncio.sleep(1)
    except Exception as e:
        logger.error(f"Error occurred while running the application: {str(e)}")
        raise e
    finally:
        if consumer is not None:
            await consumer.stop()
        await producer.stop()
        logger.info("Application has stopped")


def handle_signal(signal_number, frame):
    global is_running
    logger.info(f"Received signal {signal_number}, shutting down gracefully...")
    is_running = False


if __name__ == "__main__":
    signal.signal(signal.SIGINT, handle_signal)
    signal.signal(signal.SIGTERM, handle_signal)
    asyncio.run(main())
