from setuptools import setup, find_packages

setup(
    name="DigitalHelperHL7ToFHIR",
    version="0.1.0",
    author="Robert Smith",
    author_email="smith_robert4@me.com",
    description="Digital Helper HL7 To FHIR Microservice",
    packages=find_packages(),
    install_requires=[
        "hl7apy",
        "aiokafka",
        "confluent-kafka",
        # Add other dependencies here
    

    ],
    entry_points={
        "console_scripts": [
            "digitalhelperhl7tofhir=main:main"
        ]
    },
)
